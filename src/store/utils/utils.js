import iso_639 from '../../data/general/iso_639.json';
import axios from "axios";

function getFirstYear(array){
    var yearArray = array
        .map(a => parseInt(a.year))
        .sort((a,b) => { return a - b; })
        .filter( (value, index, self) => { return self.indexOf(value) === index;} );
    return (yearArray[0]);
}

function getLastYear(array){
    var yearArray = array
        .map(a => parseInt(a.year))
        .sort((a,b) => { return a - b; })
        .filter( (value, index, self) => { return self.indexOf(value) === index;} );
    return (yearArray[yearArray.length - 1]);
}


function countByCountry(){
    var data = []
    axios.get('award/cannes/palmedor/countByCountry')
        .then(res =>{
            data = res.data.map(e => {e.country.iso_3166_1,e.count });

        })
        .catch(error => console.log(error))
    console.log(data)
    return data;
    // var newDataArray = [];
    //
    //       var counts = array.reduce((p, c) => {
    //           console.log('c',c)
    //       var country = c.movie.production_countries
    //         if (!p.hasOwnProperty(country)) {
    //           p[country] = 0;
    //         }
    //         p[country]++;
    //         return p;
    //       }, {});
    //
    //       for(var propertyName in counts){
    //         var value = counts[propertyName];
    //         var string = propertyName;
    //         newDataArray.push([string,value]);
    //       }
    //
    //         console.log('palmedorCountByNation', newDataArray)
    //       return newDataArray;
}

function languagesCount(array){

    var containsOrIncrement = (array, name)=>{
        for(var i = 0; i < array.length; i++) {
            if (array[i].label == name) {
                array[i].count++;
                return true;
            }
        }
    };

    var languages = [];
    array.forEach((element)=>{
        element.movieDetails.movie.spoken_languages.forEach((language)=>{
            if(!containsOrIncrement(languages, language.iso_639_1)){
                languages.push({label:language.iso_639_1, count:1})
            }
        })
    })

    var orderedArray = languages.sort((a,b) => { return a.count - b.count; });

    var getLanguageFromIso639Code = (dictionary, key) =>{

        var r = key;
        dictionary.forEach(function(element) {
            if(element.code == key){
                r = element.name;
                return;
            }
        }, this);

        return r;
    };

    var languagesFormatted = [];
    orderedArray.forEach(function(element) {
        var countryName = getLanguageFromIso639Code(iso_639, element.label);
        languagesFormatted.push([countryName, element.count]);
    }, this);

    return languagesFormatted;
}

function genresCount(array){
    var containsOrIncrement = (array, name)=>{
        for(var i = 0; i < array.length; i++) {
            if (array[i].label == name) {
                array[i].count++;
                return true;
            }
        }
    };

    var languages = [];
    array.forEach((element)=>{
        element.movieDetails.movie.genres.forEach((language)=>{
            if(!containsOrIncrement(languages, language.name)){
                languages.push({label:language.name, count:1})
            }
        })
    })

    var orderedArray = languages.sort((a,b) => { return a.count - b.count; });

    var languagesFormatted = [];
    orderedArray.forEach(function(element) {
        languagesFormatted.push([element.label, element.count]);
    }, this);

    return languagesFormatted;
}

function tiedWinners(array){
    var counts = array.reduce((p, c) => {
        var year = c.year
        if (!p.hasOwnProperty(year)) {
            p[year] = 0;
        }
        p[year]++;
        return p;
    }, {});

    var newDataArray = [];
    for(var propertyName in counts){
        var value = counts[propertyName];
        var string = propertyName;
        newDataArray.push({string:string,value:value});
    }

    var maxTopMultipleWinner = newDataArray.filter((element)=>{ return element.value !== 1});

    //formatting data
    var arrayToReturn = [];
    maxTopMultipleWinner.forEach(function(element) {
        if(element.value !== 1){
            var el = element.value;
            arrayToReturn.push({string:element.string, value:element.value});
        }
    }, this);

    var orderedArray = arrayToReturn.sort((a,b) => { return parseInt(a.value) - parseInt(b.value); });
    var arrayToReturn2 = [];
    orderedArray.forEach(function(element) {
        if(element.value != 1){
            arrayToReturn2.push([element.string, element.value]);
        }
    }, this);

    return arrayToReturn2;
}

function multipleDirectorWinners(array){
    var counts = array.reduce((p, c) => {
        var directors = c.directors.join();
        if (!p.hasOwnProperty(directors)) {
            p[directors] = 0;
        }
        p[directors]++;
        return p;
    }, {});

    var newDataArray = [];
    for(var propertyName in counts){
        var value = counts[propertyName];
        var string = propertyName;
        newDataArray.push({string,value});
    }

    var orderedArray = newDataArray.sort((a,b) => { return a.value - b.value; });
    var maxValue = orderedArray[orderedArray.length-1].value;
    var maxTopMultipleWinner = orderedArray.filter((element)=>{ return element.value === maxValue});

    //formatting data
    var arrayToReturn = [];
    maxTopMultipleWinner.forEach(function(element) {
        if(element.value != 1){
            arrayToReturn.push([element.string, element.value]);
        }
    }, this);

    return arrayToReturn;
}

function accordionChunkedData(array){
    var original = array;
    var total =[];
    for(var i =0; i< original.length; i+=5){
        var chunk = [];
        for(var j=i; j< (i+5) && j<original.length ; j++){
            var item = {
                id: original[j].movie.id,
                title: original[j].movie.title,
                text: '',//''+original[j].directors.join()+' - '+original[j].year,
                url: 'https://www.imdb.com/title/'+original[j].movie.imdb_id,
                image: 'https://image.tmdb.org/t/p/w200'+ original[j].movie.poster_path
            };
            chunk.push(item);
        }
        total.push(chunk);
    }
    return total;
}

function posterWallChunkedData(array){
    var original = array.reverse();
    var total =[];
    for(var i =0; i< original.length; i+=3){
        var chunk = [];
        for(var j=i; j< (i+3) && j<original.length ; j++){

            if(original[j]["movie"]["poster_path"] === undefined){
                console.log('undefined',original["movie"])
            }
            var item = {
                id: original[j].movie.id,
                url: 'https://www.imdb.com/title/'+original[j].movie.imdb_id, //TODO: REPLACE WIKI original[j].movieLink,
                image:'https://image.tmdb.org/t/p/w400'+ original[j].movie.poster_path
            };
            chunk.push(item);
        }
        total.push(chunk);
    }
    return total;
}

function searchData(array){
    var searchArray = [];
    array.forEach(({movie})=>{
        var searchItem = {
            id: movie.id,
            title: movie.title,
            director: ['director'],
            movieLink: movie.movieLink,
            year: movie.year,
            movie_poster_small: ('https://image.tmdb.org/t/p/w200'+movie.poster_path)
        };
        searchArray.push(searchItem);
    });
    return searchArray;
}

function orderByYearDesc(array){
    return array.sort((a,b) => { return parseInt(a.year) - parseInt(b.year); });
}


export {
    getFirstYear,
    getLastYear,
    countByCountry,
    tiedWinners,
    multipleDirectorWinners,
    accordionChunkedData,
    posterWallChunkedData,
    searchData,
    getModalDataByIdAndAward,
    orderByYearDesc,
    languagesCount,
    genresCount
};
