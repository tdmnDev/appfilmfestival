import axios from 'axios'


const state={
    name: {},
    awards: [],
    posters: []
}

const getters={
    nameHomeData: state =>{
        return state.name
    },
    awardsHomeData: state =>{
        return state.awards
    },
    postersHomeData: state =>{
        return state.posters
    },
}

const mutations ={
    storeNameHomeData(state, data){
        state.name = data;
    },
    storeAwardsHomeData(state, data){
        state.awards = data;
    },
    storePostersHomeData(state, data){
        state.posters = data;
    }
}

const actions ={
    getNameHomeData({commit, state}, name){
        axios.get('/mainpage/'+name+'')
            .then(res =>{
                const data = res.data;
                commit('storeNameHomeData', data)
            })
            .catch(error => console.log(error))
    },
    getAwardsHomeData({commit, state}, name){
        axios.get('/mainpage/'+name+'/selections')
            .then(res =>{
                const data = res.data;
                console.log(data)
                commit('storeAwardsHomeData', data)
            })
            .catch(error => console.log(error))
    },
    getPostersHomeData({commit, state}, name){
        axios.get('/mainpage/'+name+'/posters')
            .then(res =>{
                const data = res.data.reverse();
                commit('storePostersHomeData', data)

            })
            .catch(error => console.log(error))
    }
}

export default {
    state,
    mutations,
    getters,
    actions
}
