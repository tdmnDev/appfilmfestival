import axios from "axios";
import {accordionChunkedData, getFirstYear, getLastYear, posterWallChunkedData, searchData} from "../utils/utils";


const state = {
    counter: 0,
    movies: [],
    countByCountry: [],
    bestAndWorst: [],
    awardHeaderDescription:{},
    menu:[]
};

const getters={
    getMovies: state=>{
        return state.movies;
    },
    getAwardHeaderDescription: state=>{
        return state.awardHeaderDescription;
    },
    countByNation: state=>{
        return state.countByCountry
    },
    palmedorFirstYear: state=>{
        return getFirstYear(state.movies);
    },
    palmedorLastYear: state=>{
        return getLastYear(state.movies);
    },
    accordionChunkedData: state=>{
        var original = state.movies;
        var total = accordionChunkedData(original);
        return total;
    },
    mobilePosterWall: state=>{
        var original = state.movies;
        var total = posterWallChunkedData(original);
        console.log(total)
        return total;
    },
    // palmedorTiedWinners: state=>{
    //     return tiedWinners(state.movies);
    // },
    // palmedorTopBudget: state=>{
    //     return bestAndWorst(state.movies,5)
    // },
    searchData: state=>{
        return searchData(state.movies);
    },
    palmedorLanguageCount: state=>{
        return languagesCount(state.movies);
    },
    // palmedorGenresCount: state=>{
    //     return genresCount(state.movies);
    // }
    getBestAndWorst: state=>{
        return state.bestAndWorst
    },
    getMenu: state=>{
        return state.menu
    }
}

const mutations={
    movies: (state, payload) =>{
        state.movies = payload;
    },
    countByCountry: (state, payload) =>{
        state.countByCountry = payload;
    },
    bestAndWorst: (state, payload) =>{
        state.bestAndWorst = payload;
    },
    awardHeaderDescription: (state, payload) =>{
        state.awardHeaderDescription = payload;
    },
    menu: (state, payload) =>{
        state.menu = payload;
    },
};

const actions={
    clearAwardData: ({commit}) =>{
        commit('movies', []);
        commit('countByCountry', []);
        commit('bestAndWorst', []);
        commit('awardHeaderDescription', [])
    },
    getAwardMovies: ({commit}, {festival, award}) =>{
        axios.get('award/'+festival+'/'+award+'')
            .then(res =>{
                const data = res.data;
                commit('movies', data)
            })
            .catch(error => console.log(error))

        axios.get('award/'+festival+'/'+award+'/countByCountry')
            .then(res =>{
                commit('countByCountry', res.data.map(e => [e.country.iso_3166_1,e.count ]))
            })
            .catch(error => console.log(error))

        axios.get('award/'+festival+'/'+award+'/bestAndWorst')
            .then(res =>{
                commit('bestAndWorst', res.data)
            })
            .catch(error => console.log(error))

        axios.get('award/'+festival+'/'+award+'/description')
            .then(res =>{
                commit('awardHeaderDescription', res.data)
            })
            .catch(error => console.log(error))


    },
    getMenuData: ({commit}) =>{
        axios.get('menu')
            .then(res =>{
                commit('menu', res.data)
            })
            .catch(error => console.log(error))
    }
};

export default {
    state,
    mutations,
    getters,
    actions
}
