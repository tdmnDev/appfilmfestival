import axios from "axios";

const state = {
    modalData: {}
};

const getters={
    modalData: state=>{
        return state.modalData;
    }
}

const mutations={
    modalData: (state, payload) =>{
        state.modalData = payload;
    }
};

const actions={
    loadModalDataByIdAndAward: ({commit}, {id, award}) =>{
        commit('modalData', {});
        axios.get('movies/'+id+'/modal')
            .then(res =>{
                console.log(res.data)
                commit('modalData', res.data);
            })
            .catch(error => console.log(error))
    }
};

export default {
    state,
    mutations,
    getters,
    actions
}
