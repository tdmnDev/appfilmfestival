import Vue from 'vue';
import Vuex from 'vuex';

import modal from './modules/shared/modal'

import * as actions from './actions';
import * as mutations from './mutations';
import * as getters from './getters';
import award from "./modules/award";
import mainPage from "./modules/mainPage";

Vue.use(Vuex);

export const store = new Vuex.Store({
    state:{

    },
    getters,
    mutations,
    actions,
    modules:{
        //Global
        modal,
        mainPage,
        award
    }
});
