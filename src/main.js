import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import {routes} from './routes'
import {store} from './store/store'
import axios from 'axios'
import Vuetify from 'vuetify'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faAward, faTrophy, faThumbsUp, faArrowUp, faArrowDown } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

Vue.use(VueRouter);
Vue.use(Vuetify, {
    iconfont: 'fa', // 'md' || 'mdi' || 'fa' || 'fa4'
    theme:  {
        primary: '#585858',
        secondary: '#4caf50',
        accent: '#009688',
        error: '#00bcd4',
        warning: '#03a9f4',
        info: '#2196f3',
        success: '#3f51b5'
    }
});

library.add(faTrophy)
library.add(faAward)
library.add(faArrowUp)
library.add(faArrowDown)
Vue.component('font-awesome-icon', FontAwesomeIcon)

axios.defaults.baseURL = 'https://filmfestivalapi.herokuapp.com'
axios.defaults.headers.get['Accepts'] = 'application/json'

const router = new VueRouter({
  routes: routes,
  mode: 'history'
});

export const eventBus = new Vue({
  methods:{
    openModal(){
      console.log('openModal')
      this.$emit('openModal')
    },
    requestModal(id){
      this.$emit('requestModal', id)
    },
    openSignupModal(){
      this.$emit('openSignupModal');
    },
    changeTopWorstBoxSize(size){
      this.$emit('changeTopWorstBoxSize', size)
    }
  }
});

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
