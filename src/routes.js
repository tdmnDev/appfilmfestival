import Header from './components/shared/Header.vue';
import Footer from './components/shared/Footer.vue';
import Main from './components/shared/MainPage.vue';
import Award from './components/shared/AwardPage.vue';
import Home from './components/Home.vue';


export const routes = [
    {
        path: '/', name: 'home', components: {
            default: Home,
            'header': Header,
            'footer': Footer
        }
    },
    {
        path: '/:festival', name: 'festival', components: {
            default: Main,
            'header': Header,
            'footer': Footer
        }
    },
    {
        path: '/:festival/:award', name: 'award', components: {
            default: Award,
            'header': Header,
            'footer': Footer
        }
    }
];


// //Lazy loading - Cannes
// const CannesHome = resolve =>{
//     require.ensure(['./components/cannes/Shared/CannesHome.vue'], ()=>{
//         resolve(require('./components/cannes/Shared/CannesHome.vue'));
//     }
// )};
// const CannesStart = resolve =>{
//     require.ensure(['./components/cannes/Shared/CannesStart.vue'], ()=>{
//         resolve(require('./components/cannes/Shared/CannesStart.vue'));
//     }
// )};
// const CannesHeader = resolve =>{
//     require.ensure(['./components/cannes/Shared/CannesHeader.vue'], ()=>{
//         resolve(require('./components/cannes/Shared/CannesHeader.vue'));
//     }
// )};
// const PalmeDor = resolve =>{
//     require.ensure(['./components/cannes/PalmeDor.vue'], ()=>{
//         resolve(require('./components/cannes/PalmeDor.vue'));
//     }
// )};
// const GrandPrix = resolve =>{
//     require.ensure(['./components/cannes/GrandPrix.vue'], ()=>{
//         resolve(require('./components/cannes/GrandPrix.vue'));
//     }
// )};
// const PrixDuJury = resolve =>{
//     require.ensure(['./components/cannes/PrixDuJury.vue'], ()=>{
//         resolve(require('./components/cannes/PrixDuJury.vue'));
//     }
// )};
// const InterpretationFeminine = resolve =>{
//     require.ensure(['./components/cannes/InterpretationFeminine.vue'], ()=>{
//         resolve(require('./components/cannes/InterpretationFeminine.vue'));
//     }
// )};
// const InterpretationMasculine = resolve =>{
//     require.ensure(['./components/cannes/InterpretationMasculine.vue'], ()=>{
//         resolve(require('./components/cannes/InterpretationMasculine.vue'));
//     }
// )};
// const MiseEnScene = resolve =>{
//     require.ensure(['./components/cannes/MiseEnScene.vue'], ()=>{
//         resolve(require('./components/cannes/MiseEnScene.vue'));
//     }
// )};
// const PrixUnCertRegard = resolve =>{
//     require.ensure(['./components/cannes/PrixUnCertRegard.vue'], ()=>{
//         resolve(require('./components/cannes/PrixUnCertRegard.vue'));
//     }
// )};
// const CameraDor = resolve =>{
//     require.ensure(['./components/cannes/CameraDor.vue'], ()=>{
//         resolve(require('./components/cannes/CameraDor.vue'));
//     }
// )};

// //Lazy loading - Berlinale
// const BerlinaleHome = resolve =>{
//     require.ensure(['./components/berlinale/Shared/BerlinaleHome.vue'], ()=>{
//         resolve(require('./components/berlinale/Shared/BerlinaleHome.vue'));
//     }
// )};
// const BerlinaleStart = resolve =>{
//     require.ensure(['./components/berlinale/Shared/BerlinaleStart.vue'], ()=>{
//         resolve(require('./components/berlinale/Shared/BerlinaleStart.vue'));
//     }
// )};
// const BerlinaleHeader = resolve =>{
//     require.ensure(['./components/berlinale/Shared/BerlinaleHeader.vue'], ()=>{
//         resolve(require('./components/berlinale/Shared/BerlinaleHeader.vue'));
//     }
// )};
// const GoldenBear = resolve =>{
//     require.ensure(['./components/berlinale/GoldenBear.vue'], ()=>{
//         resolve(require('./components/berlinale/GoldenBear.vue'));
//     }
// )};
